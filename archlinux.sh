#!/bin/bash -x

LOG_FILE=/tmp/install.log
exec > >(tee -a "$LOG_FILE") 2>&1

Starting_script(){
echo -ne "
------------------------------------------------
 █████╗ ██████╗  ██████╗██╗  ██╗████████╗██╗   ██╗    ██████╗ ██████╗ ███████╗███████╗███████╗███╗   ██╗████████╗███████╗                       
██╔══██╗██╔══██╗██╔════╝██║  ██║╚══██╔══╝██║   ██║    ██╔══██╗██╔══██╗██╔════╝██╔════╝██╔════╝████╗  ██║╚══██╔══╝██╔════╝                       
███████║██████╔╝██║     ███████║   ██║   ██║   ██║    ██████╔╝██████╔╝█████╗  ███████╗█████╗  ██╔██╗ ██║   ██║   ███████╗                       
██╔══██║██╔══██╗██║     ██╔══██║   ██║   ╚██╗ ██╔╝    ██╔═══╝ ██╔══██╗██╔══╝  ╚════██║██╔══╝  ██║╚██╗██║   ██║   ╚════██║                       
██║  ██║██║  ██║╚██████╗██║  ██║   ██║    ╚████╔╝     ██║     ██║  ██║███████╗███████║███████╗██║ ╚████║   ██║   ███████║                       
═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝     ╚═══╝      ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝                       
                                                                                                                                                
 █████╗ ██████╗  ██████╗██╗  ██╗██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     ███████╗██████╗ 
██╔══██╗██╔══██╗██╔════╝██║  ██║██║     ██║████╗  ██║██║   ██║╚██╗██╔╝    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██╔════╝██╔══██╗
███████║██████╔╝██║     ███████║██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝     ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     █████╗  ██████╔╝
██╔══██║██╔══██╗██║     ██╔══██║██║     ██║██║╚██╗██║██║   ██║ ██╔██╗     ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     ██╔══╝  ██╔══██╗
██║  ██║██║  ██║╚██████╗██║  ██║███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗███████╗██║  ██║
╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝

-------------------------------------------------

"
echo "Press Enter to continue..."
read -r

sed -i '/\s*\[multilib\]/s/^#//' /etc/pacman.conf
sed -i '/\s*\[multilib\]/,/Include = \/etc\/pacman\.d\/mirrorlist/s/^#//' /etc/pacman.conf
pacman -Syy
loadkeys sv-latin1

# Optimisations

nc=$(grep -c ^processor /proc/cpuinfo)
echo "You have " " "$nc" cores."
echo "-----------------------------------------"
echo "Changing the makeflags for '$nc' cores."
sed -i '/\s*\#MAKEFLAGS/s/^#//' /etc/makepkg.conf
sed -i 's/\(MAKEFLAGS="-j\)[0-9]\+"/\1'"$nc"'/' /etc/makepkg.conf
sed -i 's/\(MAKEFLAGS=".*\)$/\1"/' /etc/makepkg.conf

echo "Changing the compression settings for '$nc' cores."
sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T '"$nc"' -z -)/g' /etc/makepkg.conf

echo -ne "
---------------------------------------------------
- Setting up country mirrors for optimal download -
--------------------------------------------------
"

iso=$(curl -4 ifconfig.co/country-iso)
timedatectl set-ntp true
pacman -Syy --noconfirm pacman-contrib reflector rsync archlinux-keyring
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector -a 48 -c "$iso" -f 5 -l 20 --sort rate --save /etc/pacman.d/mirrorlist

echo -ne "
Installing prereqs...
"
pacman -S --noconfirm gptfdisk
}

# Defining the automatic formatting function first.

automatic_formatting(){
echo -ne "

 --------------------------------------------------
 --------    Select your disk to format    --------
 --------------------------------------------------
"
while true; do
    lsblk
    echo "Please enter disk to work on: (example /dev/sda)"
    read -r DISK

    # Check if path is valid
if [ ! -b "$DISK" ]
then
    echo "Error: Invalid disk path $DISK"
else
    # Path is valid, break out of loop
    break
fi

done

echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK!"
read -r -p "are you sure you want to continue (Y/N):" formatdisk
case $formatdisk in

y|Y|yes|Yes|YES)

echo -ne "
 --------------------------------------------------
 Formatting disk... 
 --------------------------------------------------
" | tee -a "$LOG_FILE"

# disk prep
sgdisk --zap-all --clear "${DISK}" # zap all on disk
wipefs -n "${DISK}" # Wipe ALL signatures of the disk, making it appear unformatted.
sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment

# create partitions
sgdisk -n 1:0:+1000M "${DISK}" # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:0     "${DISK}" # partition 2 (Root), default start, remaining

# set partition types
sgdisk -t 1:ef00 "${DISK}"
sgdisk -t 2:8300 "${DISK}"


# label partitions
sgdisk -c 1:"UEFISYS" "${DISK}"
sgdisk -c 2:"ROOT" "${DISK}"

echo "Done formatting disk" | tee -a "$LOG_FILE"

# make filesystems

echo -ne "
Creating Filesystems...
"
mkfs.vfat -F32 -n "UEFISYS" "${DISK}1"
mkfs.ext4 -L "ROOT" "${DISK}2"
;;

n|N|no|No|NO)

exit 1
;;


esac
}

Formatting_questions(){
echo -ne "
Do you want to format and partition the disk automatically (DESTROYS ALL DATA) or do you want to do it manually? If you are already done, just type 'done'
"

read -r -e -p "Manual, Automatic, Done [Automatic]: " FormattingAnswer
FormattingAnswer=${FormattingAnswer:-Automatic}




case $FormattingAnswer in
    Done|done)
        exit 1
    ;;

    Automatic|automatic)
    ep=1
    rp=2
    automatic_formatting
    ;;

    Manual|manual)
        lsblk
        echo "Please enter disk to work on: (example /dev/sda)"
        read -r DISK

        echo "Which partition number is EFI?"
        read -r -p "start to count from 1:" ep
    
        echo "Which partition is ROOT?"
        read -r -p "start to count from 1:" rp
    ;;
esac 
}


RestOfScript(){
echo -ne "
--------------------------------------------------
-----------------Select mountpoint----------------
--------------------------------------------------
"

echo "Please enter mountpoint to mount disks: (Example /mnt"
read -r MOUNTPOINT
echo "THIS WILL DELETE ANY EXISTING DATA IN FOLDER!"
read -r -p "are you sure you want to continue (Y/N):" mountpoint
case $mountpoint in

n|N|no|No|NO)

exit 1
;;

y|Y|yes|Yes|YES)

echo -e "\nMounting filesystems on ${MOUNTPOINT}"
mkdir -p "${MOUNTPOINT}/boot"
mount "${DISK}${rp}" "${MOUNTPOINT}"
rm -rf "${MOUNTPOINT:?}/*"

echo "EFI Partition: ${DISK}${ep}"
umount "${MOUNTPOINT}/boot/EFI" 
mkdir -p "${MOUNTPOINT}/boot/EFI"
mount "${DISK}${ep}" "${MOUNTPOINT}/boot/EFI"
rm -rf    "${MOUNTPOINT}/boot/EFI/*"

sleep 5s
;;

esac

echo -ne "
--------------------------------------------------
--------    Arch Install on Main Drive    --------
--------------------------------------------------
"
echo "Installing packages..." 
sleep 5
pacstrap "${MOUNTPOINT}/" archlinux-keyring autoconf automake bash-completion base base-devel binutils bluez dhcpcd dialog dosfstools efibootmgr flatpak fwupd gcc git grub htop kio-admin libnewt linux linux-firmware linux-headers nano networkmanager rsync sudo traceroute ufw vim wget --noconfirm --needed
genfstab -U "${MOUNTPOINT}" >> "${MOUNTPOINT}/etc/fstab"
echo "Done."
read -r -p "Press Enter to continue"
}

reboot_scripts(){
echo -ne "
--------------------------------------------------
-----------   Setting up bootloader   ------------
--------------------------------------------------
"

read -r -s -p 'What will the root password be?' rootpw

sleep 1s

tee -a /mnt/arch-chroot.sh > /dev/null <<EOT
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=Archlinux
sleep 5s
grub-mkconfig -o /boot/grub/grub.cfg
sleep 5s
echo -n "root:${rootpw}" | chpasswd
EOT

arch-chroot /mnt chmod +x /arch-chroot.sh
arch-chroot /mnt /bin/bash /arch-chroot.sh


curl https://gitlab.com/Archangelz93/ArchServer/-/raw/master/install_part2.sh -o /mnt/etc/profile.d/install_part2.sh
echo "rebooting"
sleep 10s

umount -R /mnt
reboot
}

Starting_script
Formatting_questions
RestOfScript
reboot_scripts
