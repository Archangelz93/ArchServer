#!/usr/bin/env bash
echo -ne "
Enabling network

"
systemctl enable --now NetworkManager
sleep 2s

echo -ne "
enabling multilib
"
sed -i '/\s*\[multilib\]/s/^#//' /etc/pacman.conf
sed -i '/\s*\[multilib\]/,/Include = \/etc\/pacman\.d\/mirrorlist/s/^#//' /etc/pacman.conf

echo -ne "

Enabling optimizations
"

nc=$(grep -c ^processor /proc/cpuinfo)
echo "You have " "$nc"" cores."
echo "-----------------------------------------"
echo "Changing the makeflags for '$nc' cores."
sudo sed -i 's/#MAKEFLAGS="j2"/MAKEFLAGS="j$nc"/g' /etc/makepkg.conf
echo "Changing the compression settings for '$nc' cores."
sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $nc -z -)/g' /etc/makepkg.conf


# Packagekit
read -r -p "Do you want to install packagekit to make package management possible through 'store packages?' " packagekit
case $packagekit in

    y|Y|yes|Yes|YES)
        pacman -S --noconfirm packagekit packagekit-qt5
        ;;

    n|N|no|No|NO)
        reboot_scripts
        ;;

esac

echo -ne "
creating user
"

read -r -p "What is your new username?" username
read -r -s -p 'What will the user password be?' userpw

useradd -m -G wheel "${username}"
echo -n "${username}:${userpw}" | chpasswd

echo -ne "

Done, proceeding to the new Desktop environment selection.

"
sleep 1s

microcode=$(whiptail --tail "which CPU do you have?" --menu "Select your CPU brand:" 10 30 6 \
	1 "intel" \
	2 "AMD" \
	3 "Exit"\
	3>&1 1>&2 2>&3)

case $? in
	0)
		case $microcode in
             1)
                pacman -S --noconfirm intel-ucode
                proc_ucode=intel_ucode.img
                ;;
            2)
                pacman -S --noconfirm amd-ucode
                proc_ucode=amd_ucode.img
                ;;
            *)
                echo "Skipping microcode installation."
                ;;
		esac
		;;
	*)
		echo "Whiptail command failed. Exiting."
		;;
esac


gpu=$(whiptail --title "GPU installation" --menu "Select your brand:" 10 30 6 \
	1 "Nvidia" \
	2 "AMD" \
	3 "Intel" \
	4 "Skip" \
	5 "Exit" \
	3>&1 1>&2 2>&3)

case $? in
	0)
		case $gpu in
			1)
				echo "installing nvidia"
				sudo pacman -S --noconfirm nvidia-dkms
				mkinitcpio -P
				;;
			5)
				echo "exiting script"
				exit 0
				;;
			*)
				echo "user canceled or an error occured."
				;;
		esac
		;;
	*)
		echo "Whiptail command failed. Exiting."
		;;
esac
			

desktop_choice=$(whiptail --title "Choose a Desktop Environment" --menu "There are several Desktop Environments in Linux. Choose one to install:" 15 50 6 \
    1 "GNOME" \
    2 "GNOME extra (includes some extra applications)" \
    3 "GNOME Tweaks (includes games and dev tools)" \
    4 "KDE" \
    5 "KDE and a large set of KDE applications" \
    3>&1 1>&2 2>&3)

case $? in
    0)
        case $desktop_choice in
            1)
                echo "Installing GNOME..."
                sudo pacman -S --noconfirm gnome
				systemctl enable gdm
                ;;
            2)
                echo "Installing GNOME and extra..."
                sudo pacman -S --noconfirm gnome gnome-extra
				systemctl enable gdm
                ;;
            3)
                echo "Installing GNOME, extra, and tweaks..."
                sudo pacman -S --noconfirm gnome gnome-extra gnome-tweaks
				systemctl enable gdm
                ;;
            4)
                echo "Installing KDE..."
                sudo pacman -S --noconfirm plasma
				systemctl enable sddm
                ;;
            5)
                echo "Installing KDE and applications..."
                sudo pacman -S --noconfirm plasma kde-applications
				systemctl enable sddm
                ;;
            *)
                echo "User canceled or an error occurred."
                ;;
        esac
        ;;
    *)
        echo "Whiptail command failed. Exiting."
        ;;
esac


security_choice=$(whiptail --title "Sudo configuration" --menu "Do you want to use password(Secure) or no password(less secure)?" 20 30 6 \
	1 "Password (Secure)" \
	2 "No password (Less secure)" \
	3>&1 1>&2 2>&3)

case $? in
	0)
		case $security_choice in
			1)
				sudo sed -i '/^# %wheel.*ALL=(ALL:ALL) ALL/s/^# //' /etc/sudoers
				;;
			2)
				sudo sed -i '/^# %wheel.*ALL=(ALL:ALL) NOPASSWD/s/^# //' /etc/sudoers
				;;
			*)
				echo "User canceled or an error occurred."
				;;
		esac
		;;
	*)
		echo "Whiptail command failed. Exiting."
		;;
esac

yay=$(whiptail --title "do you want to install yay?" --menu "installing yay is not neccesarily needed but recommended." 20 30 6 \
	1 "Yes (recommended)" \
	2 "No (Not recommended)" \
	3>&1 1>&2 2>&3)

case $? in
	0)
		case $yay in
		1)
			git clone https://aur.archlinux.org/yay.git
			cd yay || exit
			makepkg -si
			cd ..
			rm -r yay
			;;
		*)
			echo "skipping installation of yay."
			;;
		esac
		;;
	*)
		echo "Whiptail command failed. exiting."
		sleep 3s
		;;
esac

systemctl enable --now bluetooth

pacman -Syyu
		

rm -f /etc/systemd/system/getty@tty1.service.d/autologin.conf
rm -f /etc/profile.d/install_part2.sh
reboot
