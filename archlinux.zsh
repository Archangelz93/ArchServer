#!/usr/bin/env zsh

LOG_FILE="/tmp/install.log"
exec > >(tee -a "$LOG_FILE") 2>&1


Starting_script(){
    echo -ne "
    ------------------------------------------------
 █████╗ ██████╗  ██████╗██╗  ██╗████████╗██╗   ██╗    ██████╗ ██████╗ ███████╗███████╗███████╗███╗   ██╗████████╗███████╗                       
██╔══██╗██╔══██╗██╔════╝██║  ██║╚══██╔══╝██║   ██║    ██╔══██╗██╔══██╗██╔════╝██╔════╝██╔════╝████╗  ██║╚══██╔══╝██╔════╝                       
███████║██████╔╝██║     ███████║   ██║   ██║   ██║    ██████╔╝██████╔╝█████╗  ███████╗█████╗  ██╔██╗ ██║   ██║   ███████╗                       
██╔══██║██╔══██╗██║     ██╔══██║   ██║   ╚██╗ ██╔╝    ██╔═══╝ ██╔══██╗██╔══╝  ╚════██║██╔══╝  ██║╚██╗██║   ██║   ╚════██║                       
██║  ██║██║  ██║╚██████╗██║  ██║   ██║    ╚████╔╝     ██║     ██║  ██║███████╗███████║███████╗██║ ╚████║   ██║   ███████║                       
═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝     ╚═══╝      ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝                       
                                                                                                                                                
 █████╗ ██████╗  ██████╗██╗  ██╗██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     ███████╗██████╗ 
██╔══██╗██╔══██╗██╔════╝██║  ██║██║     ██║████╗  ██║██║   ██║╚██╗██╔╝    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██╔════╝██╔══██╗
███████║██████╔╝██║     ███████║██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝     ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     █████╗  ██████╔╝
██╔══██║██╔══██╗██║     ██╔══██║██║     ██║██║╚██╗██║██║   ██║ ██╔██╗     ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     ██╔══╝  ██╔══██╗
██║  ██║██║  ██║╚██████╗██║  ██║███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗███████╗██║  ██║
╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝

-------------------------------------------------

"
echo "Press Enter to continue or press Ctrl+C to abort..."
read -r
}

Optimisations(){
    nc=$(grep -c ^processor /proc/cpuinfo)
    # Info about core count
    echo "You have " " "$nc" cores."
    echo "-----------------------------------------"
    echo "Changing the makeflags for '$nc' cores."
    echo "-----------------------------------------"

    # Read the contents of /etc/makepkg.conf into an array
    makepkg_conf=("${(f)$(</etc/makepkg.conf)}")

    # Loop through the array and modify the MAKEFLAGS line
    for i in "${makepkg_conf[@]}"; do
        if [[ "$i" =~ ^#?makeflags=.*$ ]]; then
            makepkg_conf[(s)$i]="MAKEFLAGS=\"-j$nc\""
        fi
    done

    # Append the COMPRESSXZ line
    makepkg_conf+=("COMPRESSXZ=(xz -c -T $nc -z -)")

    # Write the modified array back to /etc/makepkg.conf
    printf '%s\n' "${makepkg_conf[@]}" > /etc/makepkg.conf
}

Installing_prequisites(){
    # Install pre-requisite packages for the script
    pacman -Syy --noconfirm btrfs-progs pacman-contrib reflector rsync archlinux-keyring gptfdisk
}

Change_mirrors(){
    # Change the mirrors to the optimal ones for the country (iso standard)
    echo "Changing the country mirrors for optimal download."
    iso=$(curl -4 ifconfig.co/country-iso)
    timedatectl set-ntp true
    # Making a backup of the mirrorlist
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
    # Updating the mirrorlist
    reflector -a 48 -c "$iso" -f 5 -l 20 --sort rate --save /etc/pacman.d/mirrorlist
    # Adding my own repository
    mirror=("Server=https://repo.archtv.org/archlinux/\$repo/os/\$arch")
    mirrorlist=("${(f)$(</etc/pacman.d/mirrorlist)}")
    printf '%s\n' "${mirror[@]}" > /etc/pacman.d/mirrorlist
}

Which_disk(){

    echo -ne "

 --------------------------------------------------
 --------    Select your disk to format    --------
 --------------------------------------------------
"
while true; do
    lsblk
    echo "Please enter disk to work on: (example /dev/sda)"
    read -r DISK

    # Check if path is valid
if [ ! -b "$DISK" ]
then
    echo "Error: Invalid disk path $DISK"
else
    # Path is valid, break out of loop
    break
fi

done


}
Partitioning_disk(){

echo -ne "
Which partition scheme do you want?
1. boot + EFI + home + root + swap (recommended)
2. boot + EFI + root + swap
3. boot + EFI + home + root
4. boot + EFI + root (Not recommended)
5. Manual
6. Done (Choose this if you have did the partitioning manually)
"

read -s "FormattingAnswer?Please enter a number [1]: "

case "$FormattingAnswer" in
    1)
     # Creating partitions
        echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK!"
        print -n "Are you sure you want to continue (Y/N): " 
        read -r formatdisk
        case $formatdisk in
            n|N|no|No|NO)
            echo "Aborting..."
            exit
            ;;

            y|Y|yes|Yes|YES)

            echo "Preparing to format your disk."
            sgdisk --zap-all --clear "${DISK}" # zap all on disk
            wipefs -n "${DISK}" # Wipe ALL signatures of the disk, making it appear unformatted.
            sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment


            # EFI partition
            echo "Creating EFI partition."
            sgdisk -n 1:0:+1G "${DISK}" # EFI partition size 1GB
            sgdisk -t 1:ef00 "${DISK}" # EFI partition type
            sgdisk -c 1:"UEFISYS" "${DISK}" # EFI partition name
            efi="1" # EFI partition path
            mkfs.vfat -F32 "${DISK}${efi}" # EFI partition format

            # boot partition
            echo "Creating boot partition."
            sgdisk -n 2:0:+2G "$DISK" # boot partition size 2GB
            sgdisk -t 2:ef02 "${DISK}" # boot partition type
            sgdisk -c 2:"Boot" "${DISK}" # boot partition name
            boot="2" # boot partition path
            mkfs.fat -F32 "${DISK}${boot}" # boot partition format

            # swap partition
            echo "Creating swap partition."
            sgdisk -n 3:0:+3G "${DISK}" # Swap partition size 3GB
            sgdisk -t 3:8200 "${DISK}" # Swap partition type
            sgdisk -c 3:"Swap" "${DISK}" # Swap partition name
            swap="3" # Swap partition path
            mkswap "${DISK}${swap}" # Swap partition format

            # root partition
            echo "Creating root partition." 
            sgdisk -n 4:0:+20G "${DISK}" # Root partition size 20GB
            sgdisk -t 4:8300 "${DISK}" # Root partition type
            sgdisk -c 4:"Root" "${DISK}" # Root partition name
            root="4" # Root partition path
            mkfs.ext4 "${DISK}${root}" # Root partition format

            # home partition
            echo "Creating home partition."
            sgdisk -n 5:0:0 "${DISK}"  # Home partition size all remaining space
            sgdisk -t 5:8300 "${DISK}" # Home partition type
            sgdisk -c 5:"Home" "${DISK}" # Home partition name
            home="5" # Home partition path

            echo -ne "
            What filesystem do you want to use for the home partition?
            1. ext4 (recommended)
            2. ext3 (Not recommended)
            3. btrfs (Not recommended)
            "
            read -r home_fs
            case $home_fs in
                1)
                    mkfs.ext4 "${DISK}${home}" # Home partition format
                    ;;
                2)
                    mkfs.ext3 "${DISK}${home}" # Home partition format
                    ;;
                3)
                    mkfs.btrfs "${DISK}${home}" -f # Home partition format
                    ;;
                *)
                    while true; do
                        echo "Invalid option, please try again."
                        read home_fs
                        # Check if choice is valid
                    if [ "$home_fs" != 1 ] && [ "$home_fs" != 2 ] && [ "$home_fs" != 3 ]
                    then
                        echo "Error: Invalid option $home_fs"
                    else
                        # Choice is valid, break out of loop
                        break
                    fi

                    done
                ;;
            esac
            ;;
        esac
        echo "----------------------------------------"
        echo "----- Your disk is now partitioned -----"
        echo "----------------------------------------"
    ;;
    2)
        # Creating partitions
        echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK!"
        print -n "Are you sure you want to continue (Y/N): "
        read -r formatdisk
        case $formatdisk in
            n|N|no|No|NO)
            echo "Aborting..."
            exit
            ;;

            y|Y|yes|Yes|YES)
        
            echo "THIS WILL DESTROY ALL DATA ON THE DISK! LAST WARNING"
            echo "Press Enter to continue or press Ctrl+C several times to abort..."
            read -r
            
            echo "Preparing to format your disk."
            sgdisk --zap-all --clear "${DISK}" # zap all on disk
            wipefs -n "${DISK}" # Wipe ALL signatures of the disk, making it appear unformatted.
            sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment
    
            # EFI partition
            echo "Creating EFI partition." 
            sgdisk -n 1:0:+1000M "${DISK}" # EFI partition size 1GB
            sgdisk -t 1:ef00 "${DISK}" # EFI partition type
            sgdisk -c 1:"UEFISYS" "${DISK}" # EFI partition name
            efi="1"
            mkfs.vfat -F32 "${DISK}${efi}" # EFI partition format
    
            # boot partition
            sgdisk -n 2:0:+2000M "${DISK}" # boot partition size 2GB
            sgdisk -t 2:ef02 "${DISK}" # boot partition type
            sgdisk -c 2:"Boot" "${DISK}" # boot partition name
            boot="2" # boot partition path
            mkfs.fat -F32 "${DISK}${boot}" # boot partition format
    
            # Swap partition
            echo "Creating swap partition." 
            sgdisk -n 3:0:+3000M "${DISK}" # Swap partition size 3GB
            sgdisk -t 3:8200 "${DISK}" # Swap partition type
            sgdisk -c 3:"Swap" "${DISK}" # Swap partition name
            swap="3" # Swap partition path
            mkswap "${DISK}${swap}" # Swap partition format
    
            # root partition
            echo "Creating root partition."
            sgdisk -n 4:0:0 "${DISK}" # Root partition size all remaining space
            sgdisk -t 4:8300 "${DISK}" # Root partition type
            sgdisk -c 4:"Root" "${DISK}" # Root partition name
            root="4" # Root partition path
            mkfs.ext4 "${DISK}${root}" # Root partition format
            ;;
        esac
    ;;
    3)
    

        # Creating partitions
        echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK!"
        print -n "Are you sure you want to continue (Y/N): "
        read -r formatdisk
        case $formatdisk in
            n|N|no|No|NO)
            echo "Aborting..."
            exit
        ;;

            y|Y|yes|Yes|YES)

            echo "THIS WILL DESTROY ALL DATA ON THE DISK! LAST WARNING"
            echo "Press Enter to continue or press Ctrl+C several times to abort..."
            read -r

            echo "Preparing to format your disk."
            sgdisk --zap-all --clear "${DISK}" # zap all on disk
            wipefs -n "${DISK}" # Wipe ALL signatures of the disk, making it appear unformatted.
            sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment

            # EFI partition
            echo "Creating EFI partition." 
            sgdisk -n 1:0:+1000M "${DISK}" # EFI partition size 1GB
            sgdisk -t 1:ef00 "${DISK}" # EFI partition type
            sgdisk -c 1:"UEFISYS" "${DISK}" # EFI partition name
            efi="1"
            mkfs.vfat -F32 "${DISK}${efi}" # EFI partition format

            # boot partition
            sgdisk -n 2:0:+2000M "${DISK}" # boot partition size 2GB
            sgdisk -t 2:ef02 "${DISK}" # boot partition type
            sgdisk -c 2:"Boot" "${DISK}" # boot partition name
            boot="2" # boot partition path
            mkfs.fat -F32 "${DISK}${boot}" # boot partition format

            # root partition
            echo "Creating root partition."
            sgdisk -n 3:0:0 "${DISK}"
            sgdisk -t 3:8300 "${DISK}" # Root partition type
            sgdisk -c 3:"Root" "${DISK}" # Root partition name
            root="3" # Root partition path
            mkfs.ext4 "${DISK}${root}" # Root partition format
            ;;
        esac
    ;;
    
    4)
        # Creating partitions
        echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK!"
        print -n "Are you sure you want to continue (Y/N): "
        read -r formatdisk
        case $formatdisk in
            n|N|no|No|NO)
            echo "Aborting..."
            exit
        ;;

            y|Y|yes|Yes|YES)

            echo "THIS WILL DESTROY ALL DATA ON THE DISK! LAST WARNING"
            echo "Press Enter to continue or press Ctrl+C several times to abort..."
            read -r

            echo "Preparing to format your disk."
            sgdisk --zap-all --clear "${DISK}" # zap all on disk
            wipefs -n "${DISK}" # Wipe ALL signatures of the disk, making it appear unformatted.
            sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment

            # EFI partition
            echo "Creating EFI partition."
            sgdisk -n 1:0:+1000M "${DISK}" # EFI partition size 1GB
            sgdisk -t 1:ef00 "${DISK}" # EFI partition type
            sgdisk -c 1:"UEFISYS" "${DISK}" # EFI partition name
            efi="1"
            mkfs.vfat -F32 "${DISK}${efi}" # EFI partition format

            # boot partition
            sgdisk -n 2:0:+2000M "${DISK}" # boot partition size 2GB
            sgdisk -t 2:ef02 "${DISK}" # boot partition type
            sgdisk -c 2:"Boot" "${DISK}" # boot partition name
            boot="2" # boot partition path
            mkfs.fat -F32 "${DISK}${boot}" # boot partition format

            # root partition
            echo "Creating root partition."
            sgdisk -n 3:0:0 "${DISK}" # Root partition size all remaining space
            sgdisk -t 3:8300 "${DISK}" # Root partition type
            sgdisk -c 3:"Root" "${DISK}" # Root partition name
            root="3" # Root partition path
            mkfs.ext4 "${DISK}${root}" # Root partition format
            ;;
            esac
    ;;
    5)
    echo "Please create the partitions manually and then run the script again. Choose done instead of automatic."
    exit
    ;;
    
    6)
    read "How many partitions do you have?"
    ;;

    *)
        echo "Invalid option $FormattingAnswer"
        Partitioning_disk
    ;;
esac
}

Select_mountpoint(){
    echo -ne "
    --------------------------------------------------
    -------- Select mountpoint to mount disks --------
    --------------------------------------------------
    "
    echo "Please enter mountpoint to mount disks: (Example /mnt)"
    read MOUNTPOINT
    echo "THIS WILL DELETE ANY EXISTING DATA IN FOLDER! (rm -rf mountpint and then mkdir -p mountpoint)"
    print -n "are you sure you want to continue (Y/N):"
    read -r mount_decision
    case $mount_decision in
        n|N|no|No|NO)
            echo "Aborting..."
            exit 1
            ;;

        y|Y|yes|Yes|YES)

        Mounting_disk
        ;;  
    esac
}

Mounting_disk(){
    rm -rf "${MOUNTPOINT}"
    sleep 1
    mkdir -p "${MOUNTPOINT}"
    sleep 1
    echo "$FormattingAnswer"
    case $FormattingAnswer in
        1)
            # Mounting disks
            echo "mounting root"
            mount "${DISK}${root}" "${MOUNTPOINT}" # Mount root partition
            echo "mounting boot"
            mkdir -p "${MOUNTPOINT}/boot" # Create boot folder
            mount "${DISK}${boot}" "${MOUNTPOINT}/boot" # Mount boot partition
            echo "mounting efi"
            mkdir -p "$MOUNTPOINT/boot/EFI" # Create EFI folder
            mount "${DISK}${efi}" "${MOUNTPOINT}/boot/EFI" # Mount EFI partition
            echo "mounting swap"
            mkdir -p "${MOUNTPOINT}/home" # Create home folder
            mount "${DISK}${home}" "${MOUNTPOINT}/home" # Mount home partition
            echo "mounting swap"
            swapon "${DISK}${swap}" # Mount swap partition
        ;;
        2)
            # Mounting disks
            mount "${DISK}${root}" "${MOUNTPOINT}" # Mount root partition
            mkdir -p "${MOUNTPOINT}/boot" # Create boot folder
            mount "${DISK}${boot}" "${MOUNTPOINT}/boot" # Mount boot partition
            mkdir -p "$MOUNTPOINT/boot/EFI" # Create EFI folder
            mount "${DISK}${efi}" "${MOUNTPOINT}/boot/EFI" # Mount EFI partition
            swapon "${DISK}${swap}" # Mount swap partition
        ;;
        3)
            # Mounting disks
            mount "${DISK}${root}" "${MOUNTPOINT}" # Mount root partition
            mkdir -p "${MOUNTPOINT}/boot" # Create boot folder
            mount "${DISK}${boot}" "${MOUNTPOINT}/boot" # Mount boot partition
            mkdir -p "$MOUNTPOINT/boot/EFI" # Create EFI folder
            mount "${DISK}${efi}" "${MOUNTPOINT}/boot/EFI" # Mount EFI partition
            mkdir -p "${MOUNTPOINT}/home" # Create home folder
            mount "${DISK}${home}" "${MOUNTPOINT}/home" # Mount home partition
        ;;
        4)
            # Mounting disks
            mount "${DISK}${root}" "${MOUNTPOINT}" # Mount root partition
            mkdir -p "${MOUNTPOINT}/boot" # Create boot folder
            mount "${DISK}${boot}" "${MOUNTPOINT}/boot" # Mount boot partition
            mkdir -p "$MOUNTPOINT/boot/EFI" # Create EFI folder
            mount "${DISK}${efi}" "${MOUNTPOINT}/boot/EFI" # Mount EFI partition
        ;;
    esac
    echo "Mounted all partitions."
}

Base_installation(){
    echo "Installing base packages."
    pacstrap "${MOUNTPOINT}/" archlinux-keyring autoconf automake bash-completion base base-devel binutils bluez dhcpcd dialog dosfstools efibootmgr flatpak fwupd gcc git grub htop kio-admin libnewt linux linux-firmware linux-headers nano networkmanager rsync sudo traceroute ufw util-linux vim wget --noconfirm --needed
    genfstab -U "${MOUNTPOINT}" >> "${MOUNTPOINT}/etc/fstab"
    echo "Done."
    echo "Press Enter to continue or press Ctrl+C to abort..."
    read -r
}

bootloader_installation(){
    echo -ne "
    ---------------------------------------
    -------- Setting up bootloader --------
    ---------------------------------------
    "
    read -s "hostname?Please enter your hostname:"
    echo "Setting hostname..."
    echo $hostname > "${MOUNTPOINT}/etc/hostname"
    echo "hostname set."

arch-chroot "${MOUNTPOINT}" /bin/bash <<EOF
MOUNTPOINT="${MOUNTPOINT}"
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=Archlinux
grub-mkconfig -o /boot/grub/grub.cfg
EOF
echo "Done installing grub."
}

Setting_up_reboot(){
    mkdir -p "${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d"
    echo -ne "[Service]
ExecStart=
ExecStart=-/usr/bin/bash --login
" >> "${MOUNTPOINT}/etc/systemd/system/getty@tty1.service.d/autologin.conf"
    
    curl "https://gitlab.com/Archangelz93/ArchServer/-/raw/master/install_part2.zsh" -o "${MOUNTPOINT}/etc/profile.d/install_part2.sh"
    echo "rebooting"
    
    umount -R "${MOUNTPOINT}"
    #reboot
}

Starting_script
Optimisations
Installing_prequisites
Change_mirrors
Which_disk
Partitioning_disk
Select_mountpoint
Base_installation
bootloader_installation
Setting_up_reboot